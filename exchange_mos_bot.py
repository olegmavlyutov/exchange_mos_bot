import json
import requests
import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from logbot import Client

TOKEN = '986671075:AAHg7OwOpW-QvngKPk66ZLB81VdfFBDaUF8'

c = Client(token=TOKEN, host="https://api.telegram.org")

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger(__name__)


def start(update, context):
    update.send_message(chat_id=context.message.chat_id, text="I'm a bot, please talk to me!")


def help(update, context):
    update.message.reply_text('Help!')


def echo(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)


def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    updater = Updater(token=TOKEN)

    dp = updater.dispatcher

    start_handler = CommandHandler('start', start)
    dp.add_handler(start_handler)

    dp.add_handler(CommandHandler("help", help))

    echo_handler = MessageHandler(Filters.text, echo)
    dp.add_handler(echo_handler)

    dp.add_error_handler(error)

    updater.start_polling()

    updater.idle()

    caps_handler = CommandHandler('caps', caps)
    dp.add_handler(caps_handler)


def caps(update, context):
    text_caps = ' '.join(context.args).upper()
    context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps)


if __name__ == '__main__':
    main()


#  начало - логика обработки JSONа
# response = requests.get("http://iss.moex.com/iss/securities.json")
# exchange_json = json.loads(response.text)
#
# securities = exchange_json['securities']
# metadata = securities['metadata']
# columns = securities['columns']
# data = securities['data']
#
# companies_list = []
#
# for i in range(len(data)):
#     companies_list.append(data[i][4])
#
# print(companies_list)
#  конец - логика обработки JSONа
